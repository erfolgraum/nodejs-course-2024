// console.log('Hello world') // Call nodeEnviroment with the command: /node index/
// console.log(global) // --> In the browser: console.log(window)
// console.log(process) // --> In the browser - DOM: comsole.log(document) 

//*** 1) ES-Modules ***
// const {generateRandomNumber, celsiusToFahrenheit} = require('./utils')

// console.log(`Random Number: ${generateRandomNumber()}`)
// console.log(`Celsius to Fahrenheit: ${celsiusToFahrenheit(15)}`)

import getPosts, {getPostsLength} from "./postController.js";

console.log(getPosts())
console.log(`Post length: ${getPostsLength()}`)

//*** 2) HTTP-Module & Create Server ***
