import http from 'http';
import dotenv from 'dotenv'

import fs from 'fs/promises'
import url from 'url'
import path from 'path'
// Load environment variables from .env file
dotenv.config()

const PORT = process.env.PORT || 8000;

// Get current path'
const __filename = url.fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)


const server = http.createServer(async (req, res)=> {
    //1
    // res.write('Hello from SERVER');
    // res.end();
    
    //2
    // res.setHeader('Content-Type', 'text/html') // to display HTML
    // res.statusCode = 404;
    // res.setHeader('Content-Type', 'text/plain') // to display text
    // res.end('<h1>Hello World from server</h1>')

    //3
    // res.writeHead(500, {'Content-Type': 'application/json'}) // to display HTML
    // res.end(JSON.stringify({message: 'Server error'}))

    //4
    // console.log(req.url)
    // console.log(req.method)

    // res.writeHead(200, {'Content-Type': 'text/html'})
    // res.end('<h1>Hello World without errors and mistakes</h1>')

    //5
    // try {
    //     //Check if GET requirest
    //     if(req.method === 'GET') {
    //         if(req.url === '/') {
    //             res.writeHead(200, {'Content-Type': 'text/html'})
    //             res.end('<h1>Homepage</h1>')
    //         } else if(req.url === '/about') {
    //             res.writeHead(200, {'Content-Type': 'text/html'})
    //             res.end('<h>About</h1>')
    //         } else {
    //             res.writeHead(404, {'Content-Type': 'text/html'})
    //             res.end('<h1>This page was not found</h1>')
    //         }
    //     } else {
    //         throw new Error('Method not allowed')
    //     }
    // } catch (error) {
    //     res.writeHead(500, {'Content-Type': 'text/plain'})
    //     res.end('Server error')
    // }

    // 6
    try{
        //Check if GET request
        if(req.method === 'GET'){
            let filePath;
            if(req.url === '/') {
                filePath = path.join(__dirname, 'public', 'index.html')
            } else if(req.url === '/about') {
                filePath = path.join(__dirname, 'public', 'about.html')
            } else {
                throw new Error('Not found')
            }

            const data = await fs.readFile(filePath);
            res.setHeader('Content-Type', 'text/html');
            res.write(data);
            res.end();
        } else {
            throw new Error('Method not allowed')
        }
    } catch(error) {
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Server Error')
    }
})

server.listen(PORT, ()=> {
    console.log(`Server running on port ${PORT}`)
})
// Call server: node server || npm start --> package.json --> "scripts": { "start": "node server.js" } 
                          //|| npm run dev --> package.json --> "scripts": { "dev": "node server.js" }
                          // Ctrl + C - stop server