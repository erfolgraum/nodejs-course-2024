import { createServer } from 'http'
import dotenv from 'dotenv'
// Load environment variables from .env file
dotenv.config()

const PORT = process.env.PORT || 8000;

const users = [
    {id: 1, name: 'John Doe'},
    {id: 2, name: 'Lidiia Doe'},
    {id: 3, name: 'Claus Doe'}
]

const server = createServer((req, res)=> {
    if(req.url === '/api/users' && req.method === 'GET') {
        res.setHeader('Content-Type', 'application/json');
        res.write(JSON.stringify(users));
        res.end()
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 404;
        res.write(JSON.stringify({message: 'Route not found'}));
        res.end();
    }
}) 

server.listen(PORT, ()=> {
    console.log(`Server running on port ${PORT}`)
})